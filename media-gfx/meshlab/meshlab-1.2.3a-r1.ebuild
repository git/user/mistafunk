# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils qt4-r2

DESCRIPTION="MeshLab is a mesh processing system for the editing of large unstructured 3D triangular meshes."
HOMEPAGE="http://meshlab.sourceforge.net/"
SRC_URI="mirror://sourceforge/meshlab/meshlab/MeshLab%20v1.2.3/MeshLabSrc_AllInc_v123a.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
DEPEND="
	>=dev-cpp/muParser-1.32-r1
	>=media-libs/glew-1.5.1
	>=media-libs/lib3ds-1.3.0
	>=media-libs/qhull-2003.1-r1
	>=x11-libs/qt-core-4.4
	x11-libs/qt-opengl"
RDEPEND="${DEPEND}"

#MY_PV=${PV//./}
#S="${WORKDIR}/MeshLabSrc_AllInc_v${MY_PV}"

S="${WORKDIR}"

EPATCH_OPTS="-p1"
PATCHES=(
	"${FILESDIR}/${PF}-use-system-libs-rpath.patch"
)

src_configure() {
	cd "${S}"/meshlab/src/external/levmar-2.3
	eqmake4 levmar-2.3.pro

	cd "${S}"/meshlab/src/external/muparser_v130/src
	eqmake4 src.pro

	cd "${S}"/meshlab/src
	eqmake4 meshlabv12.pro
}

src_compile() {
	cd "${S}"/meshlab/src/external/levmar-2.3
	emake || die "emake in levmar-2.3 failed"

	cd "${S}"/meshlab/src/external/muparser_v130/src
	emake || die "emake in muparser_v130 failed"

	cd "${S}"/meshlab/src
	emake || die "emake in src failed"
}

src_install() {
	# upstream does not provide any installation tools
	# the following is reverse engineered

	dodir /usr/lib/meshlab

	exeinto /usr/lib/meshlab
	doexe "meshlab/src/distrib/meshlab"
	doexe "meshlab/src/distrib/meshlabserver"

	doexe "meshlab/src/distrib/libcommon.so.1.0.0"
	dosym libcommon.so.1.0.0 /usr/lib/meshlab/libcommon.so.1
	dosym libcommon.so.1 /usr/lib/meshlab/libcommon.so

	dodir /usr/lib/meshlab/plugins
	exeinto /usr/lib/meshlab/plugins
	for f in "meshlab/src/distrib/plugins/*.so"
	do
		doexe $f
	done

	dodir /usr/lib/meshlab/shaders
	insinto /usr/lib/meshlab/shaders
	for f in "meshlab/src/distrib/shaders/*"
	do
		doins -r $f
	done

	cat > ${T}/meshlab <<-EOF
#!/bin/bash
/usr/lib/meshlab/meshlab
EOF

	cat > ${T}/meshlabserver <<-EOF
#!/bin/bash
/usr/lib/meshlab/meshlabserver
EOF

	exeinto /usr/bin
	doexe "${T}/meshlab"
	doexe "${T}/meshlabserver"
}
