# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# TODO: use versionator; first talk to upstream to cleanup their naming
# TODO: add slot for version 2, append -${SLOT}

EAPI=2

inherit eutils multilib qt4-r2 versionator

DESCRIPTION="A mesh processing system for the editing of large unstructured 3D triangular meshes."
HOMEPAGE="http://meshlab.sourceforge.net/"
SRC_URI="mirror://sourceforge/meshlab/meshlab/MeshLab%20v1.2.3/MeshLabSrc_AllInc_v123a.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
DEPEND="media-libs/glew
	sci-libs/levmar
	media-libs/lib3ds
	>=media-libs/qhull-2003.1-r1
	x11-libs/qt-core:4
	x11-libs/qt-opengl:4"
RDEPEND="${DEPEND}"

S=${WORKDIR}/meshlab/src

PATCHES=(
	"${FILESDIR}/${P}-use-system-libs-rpath.patch"
)

src_configure() {
	# NOTE: we keep the bundled libs for now, they are quite small in space and cpu time
	eqmake4 external/levmar-2.3/levmar-2.3.pro -o "${S}"/external/levmar-2.3/Makefile
	eqmake4 external/muparser_v130/src/src.pro -o "${S}"/external/muparser_v130/src/Makefile
	eqmake4 meshlabv12.pro
}

src_compile() {
	emake -C external/levmar-2.3 || die
	emake -C external/muparser_v130/src || die
	emake || die
}

src_install() {
	local my_libdir=/usr/$(get_libdir)/meshlab

	exeinto ${my_libdir}
	doexe distrib/{libcommon.so.1.0.0,meshlab{,server}} || die
	dosym libcommon.so.1.0.0 ${my_libdir}/libcommon.so.1 || die
	dosym libcommon.so.1 ${my_libdir}/libcommon.so || die
	dosym ${my_libdir}/meshlab /usr/bin/meshlab || die
	dosym ${my_libdir}/meshlabserver /usr/bin/meshlabserver || die

	exeinto ${my_libdir}/plugins
	doexe distrib/plugins/*.so || die

	insinto ${my_libdir}/shaders
	doins -r distrib/shaders/* || die
}
