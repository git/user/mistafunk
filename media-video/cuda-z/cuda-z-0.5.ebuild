# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils multilib qt4-r2 versionator

DESCRIPTION="CUDA Information Utility for nVIDIA chipsets."
HOMEPAGE="http://cuda-z.sourceforge.net/"
SRC_URI="mirror://sourceforge/cuda-z/${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
DEPEND="x11-drivers/nvidia-drivers
	dev-util/nvidia-cuda-toolkit
	x11-libs/qt-core:4
	x11-libs/qt-gui:4"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${PF}-fix-cuda-path.patch"
)

src_install() {
	dobin bin/${PN}
}
