# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils multilib

DESCRIPTION="GNU MCSim is a Monte-Carlo simulation package."
HOMEPAGE="http://www.gnu.org/s/mcsim"
SRC_URI="http://www.gnu.org/s/mcsim/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
  emake DESTDIR="${D}" install || die
}
