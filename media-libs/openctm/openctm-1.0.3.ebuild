# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# TODO: unbundle tinyxml, glew, etc...

EAPI=2

inherit eutils multilib qt4-r2 versionator

MY_PF=OpenCTM-1.0.3

DESCRIPTION="OpenCTM - the Open Compressed Triangle Mesh file format - is a file format, a software library and a tool set for compression of 3D triangle meshes."
HOMEPAGE="http://openctm.sourceforge.net"
SRC_URI="mirror://sourceforge/openctm/${MY_PF}/${MY_PF}-src.tar.bz2"


LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
DEPEND="media-libs/glew
        media-libs/freeglut"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${MY_PF}

src_configure() {
	sed -i 's/-lglut/-lglut -lGLU/g' tools/Makefile.linux
}

src_compile() {
	emake -f Makefile.linux openctm toolset
}

src_install() {
	dobin tools/bin2c
	dobin tools/ctmconv
	dobin tools/ctmbench
	dobin tools/ctmviewer
	dolib tools/libopenctm.so
}
