# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# TODO: replace the alembic_bootstrap.py with proper gentoo methods (cmake eclass)

EAPI=2

inherit eutils versionator

DESCRIPTION="Alembic is an open framework for storing and sharing 3D geometry data that includes a C++ library, a file format, and client plugins and applications."
HOMEPAGE="http://code.google.com/p/alembic"

MY_PN="Alembic"
MY_PV=$(replace_version_separator 3 '_')
MY_P="${MY_PN}_${MY_PV}"
MY_PV_NODATE=$(get_version_component_range 1-3)

SRC_URI="http://alembic.googlecode.com/files/${MY_P}.tgz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples doc"
RDEPEND=""
DEPEND=">=dev-util/cmake-2.8
	>=dev-libs/boost-1.44[static-libs]
	>=media-libs/ilmbase-1.0.1[static-libs]
	>=sci-libs/hdf5-1.8.7[-mpi,static-libs]
	doc? ( >=app-doc/doxygen-1.7.3 )"

TMPBUILD="${WORKDIR}/alembic-build"
TMPINSTALL="${WORKDIR}/alembic-install"

src_configure() {
	mkdir ${TMPBUILD} && cd ${TMPBUILD}
	S=${TMPBUILD}
	
	python ${WORKDIR}/${MY_P}/build/bootstrap/alembic_bootstrap.py \
		--disable-prman --disable-maya --disable-arnold \
		--hdf5_include_dir=/usr/include \
		--hdf5_hdf5_library=/usr/lib/libhdf5.a \
		--boost_include_dir=${LIBS}/boost_install/include \
		--boost_thread_library=/usr/lib/libboost_thread-mt.a \
		--zlib_include_dir=/usr/include \
		--zlib_library=/usr/lib/libz.a \
		--ilmbase_include_dir=/usr/include/OpenEXR \
		--ilmbase_imath_library=/usr/lib/libImath.a \
		.
}

src_compile() {
	emake
	
	if use examples; then
		cd ${S}/examples
		emake all
	fi
}

src_install() {
	mkdir ${TMPINSTALL}
	
	cd ${S}
	emake DESTDIR=${TMPINSTALL} install

	dolib.a ${TMPINSTALL}/usr/local/${PN}-${MY_PV_NODATE}/lib/static/*.a
	cp -r ${TMPINSTALL}/usr/local/${PN}-${MY_PV_NODATE}/include/Alembic ${D}/usr/include

	if use examples; then
		dobin ${TMPBUILD}/examples/bin/AbcEcho/abcecho
		dobin ${TMPBUILD}/examples/bin/AbcEcho/abcechobounds
		dobin ${TMPBUILD}/examples/bin/AbcStitcher/abcstitcher
		dobin ${TMPBUILD}/examples/bin/SimpleAbcViewer/SimpleAbcViewer
	fi

	dodoc ${WORKDIR}/${MY_P}/{ACKNOWLEDGEMENTS,FEEDBACK,LICENSE,MANIFEST,NEWS,README}.txt 

	if use doc; then
		cd ${WORKDIR}/${MY_P}
		doxygen
		dohtml -r doc/html/*
	fi
}
