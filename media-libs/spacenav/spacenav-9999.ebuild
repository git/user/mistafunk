# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit subversion

DESCRIPTION="Spacenavd is a free software replacement user-space driver (daemon), for 3Dconnexion's space-something 6dof input devices."
HOMEPAGE="http://spacenav.sourceforge.net/"
ESVN_REPO_URI="https://spacenav.svn.sourceforge.net/svnroot/spacenav/trunk"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	pushd spacenavd
	sed -i "/CFLAGS =/s/$/ ${CFLAGS}/" Makefile.in
	sed -i "/DAEMON=/s/\/local\//\//" init_script
	econf --prefix="${D}/usr" || die "configure spacenavd failed"
	popd
	pushd spnavcfg
	sed -i "/CFLAGS =/s/$/ ${CFLAGS}/" Makefile.in
	econf --prefix="${D}/usr" || die "configure spnavcfg failed"
	popd
	pushd libspnav
	sed -i "/CFLAGS =/s/$/ ${CFLAGS}/" Makefile.in
	econf --prefix="${D}/usr" || die "configure libspnav failed"
	popd
}

src_compile() {
	pushd spacenavd
	emake || die "compiling spacenavd failed"
	popd
	pushd spnavcfg
	emake || die "compiling spnavcfg failed"
	popd
	pushd libspnav
	emake || die "compilinge libspnav failed"
	popd
}

src_install() {
	pushd spacenavd
	emake install || die "install failed"
	newinitd init_script ${PN}
	popd
	pushd spnavcfg
	emake install || die "installing spnavcfg failed"
	popd
	pushd libspnav
	emake install || die "installing libspnav failed"
	popd	
}
